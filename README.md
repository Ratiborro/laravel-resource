# Laravel Resources

Пакет для максимально простого создания ресурсов для моделей Laravel.

Можно создавать ресурсы для конкретной модели, всех моделей в проекте, или даже включить автоматическое создание ресурсов для каждой создаваемой модели.

Документация Laravel по ресурсам: https://laravel.com/docs/8.x/eloquent-resources

## Installation

Установите пакет: 
```
composer require ratiborro/laravel-resource --dev
```

### Usage

Создайте ресурсы для всех моделей в проекте одной простой командой: 
```
php artisan make:resources
```
(требует подтверждения, так что не бойтесь выполнять :-))

---

Чтобы создать ресурсы (ресурс модели и ресурс коллекции) для конкретной модели:
```
php artisan make:resources ModelName
```

## Configuration

Публикация конфига:
```
php artisan vendor:publish --tag=laravel-resource-config
```
- Включайте/выключайте data-wrapping (см. документацию выше)
- Настраивайте шаблоны имен ресурсов
- Измените конфигурацию поиска всех моделей в проекте
- Включите автоматическое создание ресурсов при создании моделей

---

Редактирование шаблонов (stubs) создаваемых ресурсов:
```
php artisan stub:publish
```

## Contacts
Author: [Ratibor Korobin](mailto:ratiborro@gmail.com)
