<?php

namespace Ratiborro\LaravelResource;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\ServiceProvider;
use Ratiborro\LaravelResource\Console\Commands\LaravelResource;
use Ratiborro\LaravelResource\Providers\EventServiceProvider;

class LaravelResourceServiceProvider extends ServiceProvider
{
    public function boot()
    {
        if (config('laravel-resource.without_wrapping')) {
            JsonResource::withoutWrapping();
        }
        $this->publishes([
            __DIR__ . '/../config/laravel-resource.php' => config_path('laravel-resource.php'),
        ], 'laravel-resource-config');

        if ($this->app->runningInConsole()) {
            $this->commands([
                LaravelResource::class
            ]);
        }
        /*
        $this->mergeConfigFrom(
            __DIR__.'/../config/courier.php', 'courier'
        );

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        $this->loadTranslationsFrom(__DIR__.'/../resources/lang', 'courier');
        // package::file.line syntax: trans('courier::messages.welcome');

        $this->publishes([
            __DIR__.'/../resources/lang' => resource_path('lang/vendor/courier'),
        ]);

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'courier');
        // package::view syntax convention: view('courier::dashboard')

        $this->loadViewsFrom(__DIR__.'/../resources/views', 'courier');
        $this->publishes([
            __DIR__.'/../resources/views' => resource_path('views/vendor/courier'),
        ]);

        $this->loadViewComponentsAs('courier', [
            \Courier\Components\Alert::class,
            \Courier\Components\Button::class,
        ]);
        // <x-courier-alert /> or <x-courier::alert />

        if ($this->app->runningInConsole()) {
            $this->commands([
                InstallCommand::class,
                NetworkCommand::class,
            ]);
        }

        $this->publishes([
            __DIR__.'/../public' => public_path('vendor/courier'),
        ], 'public');

        $this->publishes([
            __DIR__.'/../config/package.php' => config_path('package.php')
        ], 'config');

        $this->publishes([
            __DIR__.'/../database/migrations/' => database_path('migrations')
        ], 'migrations');
        */
    }

    public function register()
    {
        $this->app->register(EventServiceProvider::class);
    }

}
