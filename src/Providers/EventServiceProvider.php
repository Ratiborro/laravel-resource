<?php

namespace Ratiborro\LaravelResource\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Ratiborro\LaravelResource\Listeners\MakeModelFinished;

class EventServiceProvider extends ServiceProvider
{
    protected $listen = [
        \Illuminate\Console\Events\CommandFinished::class => [
            MakeModelFinished::class
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
