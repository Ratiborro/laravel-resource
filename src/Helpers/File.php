<?php

namespace Ratiborro\LaravelResource\Helpers;

use Illuminate\Support\Collection;

class File extends \Illuminate\Support\Facades\File
{
    public static function getModels(string $path = null): Collection
    {
        $path = $path ?: config('laravel-resource.models.path');

        $models = config('laravel-resource.models.is_recursive', true)
            ? static::allFiles($path)
            : static::files($path);

        $models = collect($models)
            ->filter(function ($item) {
                $parser = new FileParser;
                $parser->parseFile($item->getPathname());

                if (!$parser->isClass() || $parser->isAbstract()) {
                    return false;
                }

                $class = $parser->getNamespace()
                    . DIRECTORY_SEPARATOR
                    . pathinfo($item->getPathname(), PATHINFO_FILENAME);
                $reflectionClass = new \ReflectionClass($class);
                return $reflectionClass->isSubclassOf(config('laravel-resource.models.is_subclass_of'));
            })
            ->map(function ($item) {
                return pathinfo($item->getPathname(), PATHINFO_FILENAME);
            })
            ->values();

        return $models;
    }
}
