<?php

namespace Ratiborro\LaravelResource\Helpers;

class FileParser
{
    protected $isClass;
    protected $isAbstract;
    protected $namespace;

    public function parseFile(string $path): bool
    {
        try {
            $tokens = token_get_all(file_get_contents($path));
        } catch (\Exception $exception) {
            return false;
        }

        foreach ($tokens as $i => $token) {
            if (isset($token[0])) {
                switch ($token[0]) {
                    case T_CLASS:
                        $this->isClass = true;
                        break; // TODO break 2;
                    case T_ABSTRACT:
                        $this->isAbstract = true;
                        break;
                    case T_NAMESPACE:
                        $j = $i + 2;
                        $this->namespace = '';
                        while (in_array($tokens[$j][0], [T_STRING, T_NS_SEPARATOR])) {
                            $this->namespace .= $tokens[$j][1];
                            $j++;
                        }
                        break;
                }
            }
        }

        return true;
    }

    public function isClass(): bool
    {
        return $this->isClass ?: false;
    }

    public function isAbstract(): bool
    {
        return $this->isAbstract ?: false;
    }

    public function getNamespace(): ?string
    {
        return $this->namespace ?: null;
    }
}
