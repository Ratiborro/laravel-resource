<?php

namespace Ratiborro\LaravelResource\Listeners;

use Illuminate\Support\Facades\Artisan;

class MakeModelFinished
{
    public function __construct()
    {
        //
    }

    public function handle($event)
    {
        if (empty($event->command) || $event->command !== 'make:model') {
            return;
        }

        $arguments = $event->input->getArguments();

        Artisan::call('make:resources', [
            'model' => isset($arguments['name']) ? $arguments['name'] : null
        ]);
    }
}
