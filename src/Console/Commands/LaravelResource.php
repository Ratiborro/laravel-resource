<?php

namespace Ratiborro\LaravelResource\Console\Commands;

use Illuminate\Console\Command;
use Ratiborro\LaravelResource\Helpers\File;

class LaravelResource extends Command
{
    protected $signature = 'make:resources {model?}';

    protected $description = 'Generate Resources for model and model collections';

    protected $config = [];

    protected $models = [];

    protected $tableRows = [];

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->config = config('laravel-resource');
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $this->models = $this->getModels();

        if (empty($this->models)) {
            $this->error('Model name can not be empty');
            return 1;
        }

        foreach ($this->models as $model) {
            $this->info("Creating resources for $model");

            if (!empty($this->config['resource']['generate'])) {
                $resourceName = $this->getResourceName($model);
                $this->createResource($resourceName);
            } else {
                $this->warn('Generation of model resource disabled in config');
            }

            if (!empty($this->config['resource_collection']['generate'])) {
                $resourceCollectionName = $this->getResourceCollectionName($model);
                $this->createResourceCollection($resourceCollectionName);
            } else {
                $this->warn('Generation of model collection resource disabled in config');
            }

            $this->tableRows[] = [
                $model,
                isset($resourceName) ? $resourceName : null,
                isset($resourceCollectionName) ? $resourceCollectionName : null
            ];
        }

        $this->table(['Model', 'Resource', 'ResourceCollection'], $this->tableRows);

        return 0;
    }

    protected function getAppModels(string $path = null): array
    {
        return File::getModels($path)->toArray();
    }

    protected function getModels(): array
    {
        $model = $this->argument('model');
        if ($model) {
            return [$model];
        }

        $models = $this->getAppModels();
        if ($this->confirm('You want to create resources for all models?')) {
            return $models;
        }

        $model = $this->askWithCompletion('Type model name', $models);
        $models = [$model];
        return array_filter($models);
    }

    protected function getResourceName(string $modelName): string
    {
        return $this->config['resource']['name']['prefix']
            . $modelName
            . $this->config['resource']['name']['postfix'];
    }

    protected function getResourceCollectionName(string $modelName): string
    {
        return $this->config['resource_collection']['name']['prefix']
            . $modelName
            . $this->config['resource_collection']['name']['postfix'];
    }

    protected function createResource(string $name)
    {
        $this->call('make:resource', [
            'name' => $name,
            '--collection' => false
        ]);
    }

    protected function createResourceCollection(string $name)
    {
        $this->call('make:resource', [
            'name' => $name,
            '--collection' => true
        ]);
    }
}
