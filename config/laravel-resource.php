<?php

return [
    'without_wrapping' => true, // Documentation: https://laravel.com/docs/8.x/eloquent-resources#data-wrapping
    'resource' => [
        'generate' => true, // Generating model resource by command make:resources
        'name' => [
            'prefix' => null,
            'postfix' => 'Resource' // Layout: <Prefix><ModelName><Postfix>, Default: <ModelName>Resource
        ]
    ],
    'resource_collection' => [
        'generate' => true, // Generating model collection resource by command make:resources
        'name' => [
            'prefix' => null,
            'postfix' => 'Collection' // Layout: <Prefix><ModelName><Postfix>, Default: <ModelName>Collection
        ]
    ],
    'models' => [
        'path' => app_path('Models'), // Searching models in this path
        'is_recursive' => true, // Searching in subdirectories
        'is_subclass_of' => \Illuminate\Database\Eloquent\Model::class // Model is a subclass of the specified class
    ],
    'auto-creating' => false // Auto start `make:resources` command for new models (created by 'make:model' command),
];
